variable "region" {
  default     = "us-east-1"
  description = "The AWS region."
}

variable "prefix" {
  default     = "levelninenet"
  description = "Organisation or project name."
}

variable "environment" {
  default     = "development"
  description = "Name of the environment."
}

output "s3_bucket_id" {
  value = aws_s3_bucket.remote_state.id
}
